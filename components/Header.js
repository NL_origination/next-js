import Link from 'next/link';
import {Flex, Text, Box, Link as RBLink} from 'rebass'


const linkStyle = {
  marginRight: 15
};

const Header = () => (
  <div>
    <Flex
      px={2}
      color='white'
      bg='black'
      alignItems='center'>
      <Text p={2} fontWeight='bold'>Rebass</Text>
      <Box mx='auto' />
      <Link href='/'>
        <RBLink variant='nav'>Home</RBLink>
      </Link>
      <Link href='/about'>
        <RBLink variant='nav'>About</RBLink>
      </Link>
    </Flex>

  </div>

);

export default Header;