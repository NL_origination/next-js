import Header from './Header';
import {Box} from 'rebass'

const layoutStyle = {
  margin: 30,
  padding: 20,
  border: '1px solid #DDD'
};

const Layout = props => (
    <Box>
    <Header />
    {props.children}
  </Box>
);

export default Layout;