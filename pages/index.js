import { useRouter } from 'next/router';
import useSWR from 'swr';
import Link from 'next/link';
import Layout from '../components/Layout';

function fetcher(url) {
  return fetch(url).then(r => r.json());
}

const PostLink = props => (
    <li>
      <Link href="/p/[id]" as={`/p/${props.id}`}>
      <a>{props.id}</a>
    </Link>
    </li>
);

export default function Index() {
  const { query } = useRouter();
  const { data, error } = useSWR(
    `/api/randomQuote${query.author ? '?author=' + query.author : ''}`,
    fetcher
  );
  // The following line has optional chaining, added in Next.js v9.1.5,
  // is the same as `data && data.author`
  const author = data?.author;
  let quote = data?.quote;

  if (!data) quote = 'Loading...';
  if (error) quote = 'Failed to fetch the quote.';

  return (     
      <>     
          <ul>
             <PostLink id="Links" />
             <PostLink id="Learning" />
          </ul>
      
      <div className="quote">{quote}</div>
      {author && <span className="author">- {author}</span>} 
      </>  
  );
}