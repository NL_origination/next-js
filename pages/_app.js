import { ThemeProvider } from 'theme-ui'
import theme from '@rebass/preset'
import Layout from '../components/Layout.js';
import Header from '../components/Header.js'

function App({ Component, pageProps }) {
    return (
    <ThemeProvider theme={theme}>   
        <Layout>            
            <Component {...pageProps} />
        </Layout>
    </ThemeProvider>
    )
  }
  
  export default App